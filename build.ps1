$ErrorActionPreference = "Continue"

Write-Output "Running build on $Env:computername ..."

& {
    # environment variables
    $PathMSBuild = "D:\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin"
    $PathSolutionFile = $env:CI_PROJECT_DIR + "\helloci\helloci.sln"

    Write-Output "PathMSBuild = $PathMSBuild"
    Write-Output "PathSolutionFile = $PathSolutionFile"

    # append path to env
    $Env:path += ";" + $PathMSBuild
    
    & "msbuild" $PathSolutionFile "/p:Configuration=Release"

    if(!$?) { Exit $LASTEXITCODE }
}

if(!$?) { Exit $LASTEXITCODE }
